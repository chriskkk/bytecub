--物模型unique key更改为产品+identifier,取消对func_type的约束
ALTER TABLE `t_product_func`
  DROP INDEX `UNIQUE_PR_ID`,
  ADD UNIQUE INDEX `UNIQUE_PR_ID` (`product_code`, `identifier`);

-- 固件管理列表 --
CREATE TABLE `t_firmware` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '固件地址',
  `firmware_version` varchar(30) NOT NULL DEFAULT '' COMMENT '固件版本',
  `seq_no` int(11) NOT NULL DEFAULT '0' COMMENT '固件序列号(和当前版本对比)',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `product_code` varchar(64) NOT NULL DEFAULT '' COMMENT '所属产品',
  `del_flag` int(11) NOT NULL DEFAULT '0' COMMENT '删除标记',
  `sign_type` varchar(20) NOT NULL DEFAULT 'md5' COMMENT '签名类型',
  `sign_code` varchar(255) NOT NULL DEFAULT '' COMMENT '签名内容',
  `firmware_name` varchar(30) NOT NULL DEFAULT '' COMMENT '固件名称',
  `push_type` int(11) NOT NULL DEFAULT '1' COMMENT '1:url升级 2:二进制升级 3: 混合升级',
  PRIMARY KEY (`id`),
  UNIQUE KEY `index_version` (`firmware_version`,`product_code`),
  UNIQUE KEY `index_seq` (`seq_no`,`product_code`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=utf8mb4;

CREATE TABLE `t_firmware_task` (
`id` bigint(20) unsigned NOT NULL AUTO_INCREMENT COMMENT '主键',
  `task_name` varchar(60) NOT NULL DEFAULT '' COMMENT '任务名称',
  `firmware_id` bigint(20) unsigned NOT NULL COMMENT '关联固件ID',
  `upgrade_type` int(11) NOT NULL DEFAULT '1' COMMENT '1:指定设备 2:产品级别',
  `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `task_desc` varchar(255) NOT NULL DEFAULT '',
  `device_amount` int(11) NOT NULL DEFAULT '0' COMMENT '选中的设备总数',
  `del_flag` int(11) NOT NULL DEFAULT '0',
  `book_time` timestamp NULL DEFAULT NULL COMMENT '预定时间升级',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;


CREATE TABLE `t_firmware_task_detail` (
                                          `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
                                          `task_id` bigint(20) unsigned NOT NULL DEFAULT '0',
                                          `device_code` varchar(64) NOT NULL DEFAULT '' COMMENT '设备编码',
                                          `upgrade_status` int(11) NOT NULL DEFAULT '0' COMMENT '0:等待升级 1:已发送设备 2:设备收到  3:升级成功 4:升级失败',
                                          `create_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                                          `update_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
                                          `detail_msg` varchar(100) NOT NULL DEFAULT '' COMMENT '描述',
                                          `message_id` varchar(100) NOT NULL DEFAULT '' COMMENT '消息ID',
                                          PRIMARY KEY (`id`),
                                          UNIQUE KEY `UNI` (`message_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;