package com.bytecub.udp.service.parser;

import com.bytecub.common.constants.BCConstants;
import com.bytecub.udp.domain.bo.ParseDeviceBo;
import com.bytecub.utils.JSONProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.Map;

/**
 * Created on 2021/8/25.
 * 解析包头，获取deviceCode以及是否上报网关子设备数据标识
 * @author songbin songbin.sky@hotmail.com
 */
@Slf4j
@Service
public class HeaderParser {
    public ParseDeviceBo parseBody(byte[] body){
        String deviceCode = this.parseDeviceCode(body);
        Boolean subFlag = this.parseSubFlag(body);
        ParseDeviceBo parseDeviceBo = new ParseDeviceBo();
        parseDeviceBo.setDeviceCode(deviceCode);
        parseDeviceBo.setSubFlag(subFlag);
        return parseDeviceBo;
    }


    /**
     * 本方法内把所有可能获取deviceId的方式都试一遍，
     * 当然这里可以能会有优先级，比如同时匹配clientId和imei，那根据自己的业务决定到底以哪个为准
     * */
    private String parseDeviceCode(byte[] content){
        String deviceCode = this.parseWatch(content);
        if(!StringUtils.isEmpty(deviceCode)){
            return deviceCode;
        }

        deviceCode = this.parseSwitch(content);

        return deviceCode;
    }
    /**
     * 这样的函数可以多写几个，在这里把可能获取到设备编码的格式都解析一遍
     * */
    private String parseWatch(byte[] content){
        try{
            String str = new String(content, BCConstants.GLOBAL.CHARSET_UFT8);
            Map<String, Object> map = JSONProvider.parseObject(str, Map.class);
            return (String)map.get("imei");
        }catch (Exception e){
            log.warn("UDP从原始数据获取设备编码异常");
            return null;
        }
    }

    private String parseSwitch(byte[] content){
        try{
            String str = new String(content, BCConstants.GLOBAL.CHARSET_UFT8);
            Map<String, Object> map = JSONProvider.parseObject(str, Map.class);
            return (String)map.get("clientId");
        }catch (Exception e){
            log.warn("UDP从原始数据获取设备编码异常");
            return null;
        }
    }
    /**
     * 判断是否为网关设备在上报子设备消息
     * */
    private Boolean parseSubFlag(byte[] content){
        return false;
    }
}
