package com.bytecub.udp.domain.bo;

import com.bytecub.protocol.base.IBaseProtocol;
import lombok.Data;

/**
 *  * ByteCub.cn.
 *  * Copyright (c) 2020-2021 All Rights Reserved.
 *  * 
 *  * @author bytecub@163.com  songbin
 *  * @Date 2021/4/2  Exp $$
 *  
 */
@Data
public class ParseDeviceBo {
    private IBaseProtocol baseProtocol;
    private String deviceCode;
    private String productCode;
    /**
     * 用来标记此条消息是否为网关设备在上报子设备的消息
     * */
    private Boolean subFlag = false;
}
