package com.bytecub.gateway.mq.services.impl;

import com.bytecub.common.biz.TopicBiz;
import com.bytecub.common.domain.dto.response.device.DevicePageResDto;
import com.bytecub.common.domain.message.DeviceDownMessage;
import com.bytecub.common.enums.BCErrorEnum;
import com.bytecub.common.enums.DevTypeEnum;
import com.bytecub.common.enums.TopicTypeEnum;
import com.bytecub.common.exception.BCGException;
import com.bytecub.gateway.mq.domain.TopicMsgBo;
import com.bytecub.gateway.mq.services.PluginHelperService;
import com.bytecub.mdm.dao.po.ProductPo;
import com.bytecub.mdm.service.IDeviceService;
import com.bytecub.mdm.service.IProductService;
import com.bytecub.protocol.base.IBaseProtocol;
import com.bytecub.protocol.service.IProtocolUtilService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created on 2021/8/24.
 *
 * @author songbin songbin.sky@hotmail.com
 */
@Slf4j
@Service
public class PluginHelperServiceImpl implements PluginHelperService {
    @Autowired
    private IProductService productService;
    @Autowired
    private IProtocolUtilService protocolUtilService;
    @Autowired
    private IDeviceService deviceService;
     /**
      * 如果是网关子设备的话需要topic就是对应网关设备的topic，且下发的topic需要以/sub结尾
      * */
    @Override
    public TopicMsgBo encode(TopicTypeEnum topicTypeEnum, String deviceCode, DeviceDownMessage deviceDownMessage) {
        byte[] msg = new byte[0];
        String topic = "";
        TopicMsgBo topicMsgBo = new TopicMsgBo();
        DevicePageResDto deviceDto = deviceService.queryByDevCode(deviceCode);
        if(DevTypeEnum.SUB.getType().equals(deviceDto.getNodeType())){
            //子设备
            String gwDeviceCode = deviceDto.getGwDevCode();
            /**网关设备产品编码*/
            DevicePageResDto gwDeviceDto = deviceService.queryByDevCode(gwDeviceCode);
            String gwDeviceProductCode = gwDeviceDto.getProductCode();
            deviceDownMessage.setDeviceCode(deviceCode);
            deviceDownMessage.setGwDeviceCode(gwDeviceCode);
            deviceDownMessage.setSubFlag(true);
            IBaseProtocol baseProtocol = this.queryProtocolByProductCode(deviceDto.getProductCode());
            topic = TopicBiz.buildTopicByType(gwDeviceCode, gwDeviceProductCode, topicTypeEnum);
            msg = baseProtocol.encode(topic, deviceCode, deviceDownMessage);
        }else{
            IBaseProtocol baseProtocol = this.queryProtocolByProductCode(deviceDto.getProductCode());
            topic = TopicBiz.buildTopicByType(deviceCode, deviceDownMessage.getProductCode(), topicTypeEnum);
            msg = baseProtocol.encode(topic, deviceCode, deviceDownMessage);

        }
        topicMsgBo.setTopic(topic);
        topicMsgBo.setMsg(msg);
        return topicMsgBo;
    }


    /** 根据产品编码查询协议实现 */
    private IBaseProtocol queryProtocolByProductCode(String productCode) {
        try {
            ProductPo productPo = productService.queryByCode(productCode);
            return protocolUtilService.queryProtocolInstanceByCode(productPo.getProtocolCode());
        } catch (Exception e) {
            log.warn("", e);
            throw BCGException.genException(BCErrorEnum.INNER_EXCEPTION);
        }
    }
}
