package com.bytecub.mdm.cache.impl;

import com.bytecub.common.biz.RedisKeyUtil;
import com.bytecub.common.constants.BCConstants;
import com.bytecub.common.domain.gateway.mq.DeviceActiveMqBo;
import com.bytecub.common.enums.BatchOpEnum;
import com.bytecub.mdm.cache.IDeviceOfflineCache;
import com.bytecub.plugin.redis.CacheTemplate;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.*;

/**
 *  * ByteCub.cn.
 *  * Copyright (c) 2020-2021 All Rights Reserved.
 *  * 
 *  * @author bytecub@163.com  songbin
 *  * @Date 2021/3/12  Exp $$
 *  
 */
@Service
@Slf4j
public class DeviceOfflineCacheImpl implements IDeviceOfflineCache {
    @Autowired
    private  CacheTemplate cacheTemplate;
    @Value("${bytecub.device.ping.expire:120000}")
    private Integer expired;

    @Override
    public void cacheWriter(DeviceActiveMqBo deviceActiveMqBo) {
        String key = this.buildKey(deviceActiveMqBo.getDeviceCode());
        if(!deviceActiveMqBo.getActive()){
            cacheTemplate.remove(key);
            cacheTemplate.zRem(BCConstants.REDIS_KEY.DEVICE_ONLINE, deviceActiveMqBo.getDeviceCode());
        }else {
            cacheTemplate.set(key, deviceActiveMqBo, expired/1000);
            Double score = new Double(System.currentTimeMillis()/1000);
            cacheTemplate.zAdd(BCConstants.REDIS_KEY.DEVICE_ONLINE, score, deviceActiveMqBo.getDeviceCode());
        }

    }

    @Override
    public DeviceActiveMqBo cacheReader(String deviceCode) {
        try{
            String key = this.buildKey(deviceCode);
            DeviceActiveMqBo deviceActiveMqBo = cacheTemplate.get(key, DeviceActiveMqBo.class);
            if(null == deviceActiveMqBo){
                return null;
            }
            Long now = System.currentTimeMillis();
            Long timeout = now - deviceActiveMqBo.getTimestamp().getTime();
            if(timeout > expired){
                cacheTemplate.remove(key);
                return null;
            }
            return deviceActiveMqBo;
        }catch (Exception e){
            log.warn("读缓存异常", e);
            return null;
        }

    }

    @Override
    public Long activeCount() {
        return cacheTemplate.zcard(BCConstants.REDIS_KEY.DEVICE_ONLINE);
    }

    @Override
    public Map<String, DeviceActiveMqBo> cacheMapReader(List<String> deviceCodes) {
        Map<String, DeviceActiveMqBo> map = new HashMap<>();
        List<DeviceActiveMqBo> list = this.cacheBatchReader(deviceCodes);
        for(DeviceActiveMqBo item:list){
            map.put(item.getDeviceCode(), item);
        }
        return map;
    }

    @Override
    public List<DeviceActiveMqBo> cacheBatchReader(List<String> deviceCodes) {
        if(CollectionUtils.isEmpty(deviceCodes)){
            return null;
        }
        List<String> keys = new ArrayList<>();
        for(String item: deviceCodes){
            String key = this.buildKey(item);
            keys.add(key);
        }
        List<DeviceActiveMqBo> list = cacheTemplate.mget(keys, DeviceActiveMqBo.class);

        return list;
    }

    @Override
    public void cacheBatchWriter(List<String> deviceCodes, BatchOpEnum batchOpEnum) {
        if(CollectionUtils.isEmpty(deviceCodes)){
            return ;
        }
        for(String deviceCode : deviceCodes){
            DeviceActiveMqBo bo = new DeviceActiveMqBo();
            bo.setDeviceCode(deviceCode);
            Boolean activeStatus = batchOpEnum.equals(BatchOpEnum.ONLINE) ? true : false;
            bo.setActive(activeStatus);
            this.cacheWriter(bo);
        }

    }

    @Override
    public List<String> removeExpire() {
        List<String> lst = new ArrayList<>();
        try{

            Long now = System.currentTimeMillis()/1000;
            Double max = new Double(now - expired/1000);
            Set<String> sets = cacheTemplate.zRangeByScore(BCConstants.REDIS_KEY.DEVICE_ONLINE, 0, max);
            cacheTemplate.zRemRangeByScore(BCConstants.REDIS_KEY.DEVICE_ONLINE, 0, max);

            if(CollectionUtils.isEmpty(sets)){
                return lst;
            }
            for(String deviceCode: sets){
                String key = this.buildKey(deviceCode);
                lst.add(deviceCode);
                cacheTemplate.remove(key);
            }
            return lst;
        }catch (Exception e){
            log.warn("移除超时设备异常", e);
            return lst;
        }

    }

    private final String buildKey(String deviceCode){
        return  RedisKeyUtil.buildDeviceOfflineKey(deviceCode);
    }
}
