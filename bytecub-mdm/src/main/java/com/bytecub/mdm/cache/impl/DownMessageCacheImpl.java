package com.bytecub.mdm.cache.impl;

import com.bytecub.common.constants.BCConstants;
import com.bytecub.common.domain.message.DeviceDownMessage;
import com.bytecub.mdm.cache.IDownMessageCache;
import com.bytecub.plugin.redis.CacheTemplate;
import com.bytecub.utils.JSONProvider;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 *  * ByteCub.cn.
 *  * Copyright (c) 2020-2021 All Rights Reserved.
 *  * 
 *  * @author bytecub@163.com  songbin
 *  * @Date 2021/4/6  Exp $$
 *  
 */
@Service
@Slf4j
public class DownMessageCacheImpl implements IDownMessageCache {
    @Autowired
    private CacheTemplate cacheTemplate;
    @Override
    public void downMessageWriter(DeviceDownMessage content) {
        String key = this.buildKey(content.getDeviceCode());
        String field =  content.getIdentifier();
        cacheTemplate.addHashMap(key, field, JSONProvider.toJSONString(content));
        cacheTemplate.expire(key, 60*60*24);
    }

    @Override
    public List<DeviceDownMessage> downMessageReader(String deviceCode) {
        List<DeviceDownMessage> list = new ArrayList<>();
        String redisKey = this.buildKey(deviceCode);
        Map<String, String> map = cacheTemplate.getHashMapAll(redisKey);
        if(CollectionUtils.isEmpty(map)){
            return list;
        }
        map.forEach((key, value) ->{
            DeviceDownMessage item = JSONProvider.parseObject(value, DeviceDownMessage.class);
            if(null != item){
                list.add(item);
            }
        });
        cacheTemplate.remove(redisKey);
        return list;
    }

    private String buildKey(String deviceCode){
        return BCConstants.REDIS_KEY.DOWN_MSG_UDP + deviceCode;
    }
}
