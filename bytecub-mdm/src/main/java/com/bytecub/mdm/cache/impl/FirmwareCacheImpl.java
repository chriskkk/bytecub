package com.bytecub.mdm.cache.impl;

import com.bytecub.common.constants.BCConstants;
import com.bytecub.mdm.cache.IFirmwareCache;
import com.bytecub.plugin.redis.CacheTemplate;
import com.bytecub.utils.file.Base64FileUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

/**
 * com.bytecub.mdm.cache.impl
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/13
 */
@Slf4j
@Service
public class FirmwareCacheImpl implements IFirmwareCache {

    @Autowired
    private CacheTemplate cacheTemplate;

    @Override
    public String writer(Long firmwareId, String uri) {
        String key = this.buildKey(firmwareId);
        String url =  uri;
        String baseFile = Base64FileUtil.http2base64(url);
        baseFile = baseFile.replaceAll("\r|\n", "");
        cacheTemplate.set(key, baseFile);
        cacheTemplate.expire(key, 60*60*12);
        return baseFile;
    }

    @Override
    public String reader(Long firmwareId) {
        String key = this.buildKey(firmwareId);
        return cacheTemplate.get(key);
    }

    @Override
    public void remove(Long firmwareId) {
        String key = this.buildKey(firmwareId);
        cacheTemplate.remove(key);
    }

    private String buildKey(Long  firmId){
        return BCConstants.REDIS_KEY.FIRMWARE_FILE + firmId;
    }
}
