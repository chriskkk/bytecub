package com.bytecub.mdm.service.impl;

import java.util.List;

import com.bytecub.common.domain.DataResult;
import com.bytecub.common.domain.dto.request.DevQueryReqDto;
import com.bytecub.common.domain.dto.request.firmware.FirmwareCreateReqDto;
import com.bytecub.common.domain.dto.request.upgrade.UpgradeDeviceQueryReqDto;
import com.bytecub.common.domain.dto.response.device.DevicePageResDto;
import com.bytecub.common.enums.BCErrorEnum;
import com.bytecub.common.exception.BCGException;
import com.bytecub.mdm.cache.IFirmwareCache;
import com.bytecub.mdm.dao.dal.FirmwareTaskDetailPoMapper;
import com.bytecub.mdm.dao.po.FirmwareTaskDetailPo;
import com.bytecub.mdm.service.IDeviceService;
import com.github.pagehelper.PageHelper;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import com.bytecub.common.domain.dto.PageReqDto;
import com.bytecub.common.domain.dto.PageResDto;
import com.bytecub.common.domain.dto.request.firmware.FirmwareQueryReqDto;
import com.bytecub.mdm.dao.dal.FirmwarePoMapper;
import com.bytecub.mdm.dao.po.FirmwarePo;
import com.bytecub.mdm.service.IFirmwareService;

import javax.xml.ws.ServiceMode;

/**
 * com.bytecub.mdm.service.impl
 * project bytecub
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/8
 */
@Service
@Slf4j
public class FirmwareServiceImpl implements IFirmwareService {
    @Autowired
    private FirmwarePoMapper firmwarePoMapper;
    @Autowired
    private IFirmwareCache firmwareCache;
    @Autowired
    private IDeviceService deviceService;
    @Autowired
    private FirmwareTaskDetailPoMapper taskDetailPoMapper;

    @Override
    public PageResDto<FirmwarePo> searchByPage(PageReqDto<FirmwareQueryReqDto> pageReqDto) {
        int startId = (pageReqDto.getPageNo() - 1) * pageReqDto.getLimit();
        FirmwareQueryReqDto query = pageReqDto.getParamData();
        String firmwareName = StringUtils.isEmpty(query.getFirmwareName()) ? null : query.getFirmwareName();
        String productCode = StringUtils.isEmpty(query.getProductCode()) ? null : query.getProductCode();
        query.setFirmwareName(firmwareName);
        query.setProductCode(productCode);
        List<FirmwarePo> list = firmwarePoMapper.queryByPage(query, startId, pageReqDto.getLimit());
        Long total = firmwarePoMapper.countByPage(query);
        PageResDto<FirmwarePo> resultPage =
                PageResDto.genResult(pageReqDto.getPageNo(), pageReqDto.getLimit(), total, list, null);
        return resultPage;
    }

    @Override
    public void create(FirmwarePo po) {
        FirmwarePo query = new FirmwarePo();
        query.setProductCode(po.getProductCode());
        query.setFirmwareVersion(po.getFirmwareVersion());

        List<FirmwarePo> list = firmwarePoMapper.select(query);
        if(!CollectionUtils.isEmpty(list)){
            throw BCGException.genException(BCErrorEnum.FIRMWARE_VERSION_UNIQUE_ERROR);
        }

        FirmwarePo querySeq = new FirmwarePo();
        querySeq.setProductCode(po.getProductCode());
        querySeq.setSeqNo(po.getSeqNo());
        list = firmwarePoMapper.select(querySeq);
        if(!CollectionUtils.isEmpty(list)){
            throw BCGException.genException(BCErrorEnum.FIRMWARE_SEQ_UNIQUE_ERROR);
        }
        firmwarePoMapper.insertSelective(po);
    }

    @Override
    public void updateById(FirmwarePo po) {
        po.setUpdateTime(null);
        po.setCreateTime(null);
        po.setDelFlag(null);
        po.setSignType(null);
        firmwareCache.remove(po.getId());
        firmwarePoMapper.updateByPrimaryKeySelective(po);
    }

    @Override
    public void deleteById(Long id) {
        firmwarePoMapper.deleteById(id);
    }

    @Override
    public FirmwarePo queryById(Long id) {
        return firmwarePoMapper.queryById(id);
    }

    @Override
    public List<DevicePageResDto> deviceSearch(UpgradeDeviceQueryReqDto reqDto) {
        FirmwarePo firmwarePo = this.queryById(reqDto.getFirmwareId());
        if(null == firmwarePo){
            throw BCGException.genException(BCErrorEnum.DATA_EMPTY);
        }
        PageReqDto<DevQueryReqDto> pageReqDto = new PageReqDto<>();
        DevQueryReqDto queryReqDto = new DevQueryReqDto();
        queryReqDto.setEnableStatus(1);
        queryReqDto.setProductCode(firmwarePo.getProductCode());
        queryReqDto.setDeviceName(reqDto.getDeviceName());
        queryReqDto.setDeviceCode(reqDto.getDeviceCode());
        pageReqDto.setLimit(10);
        pageReqDto.setPageNo(1);
        pageReqDto.setParamData(queryReqDto);

        PageResDto<DevicePageResDto> resDtoPageResDto = deviceService.queryByPage(pageReqDto);
        return resDtoPageResDto.getResultData();
    }

    @Override
    public PageResDto<FirmwareTaskDetailPo> taskDetail(PageReqDto<FirmwareTaskDetailPo> searchPage) {
        Integer pageNo = searchPage.getPageNo();
        Integer limit = searchPage.getLimit();
        FirmwareTaskDetailPo param = searchPage.getParamData();
        FirmwareTaskDetailPo query = new FirmwareTaskDetailPo();
        //query.setTaskId(param.getTaskId());
        String deviceCode = com.bytecub.utils.StringUtils.isEmpty(param.getDeviceCode()) ? null :param.getDeviceCode();
        query.setDeviceCode(deviceCode);
        String taskName = com.bytecub.utils.StringUtils.isEmpty(param.getTaskName()) ? null : param.getTaskName();
        query.setTaskName(taskName);
        String firmwareName = com.bytecub.utils.StringUtils.isEmpty(param.getFirmwareName()) ? null : param.getFirmwareName();
        query.setFirmwareName(firmwareName);
        PageHelper.startPage(pageNo, limit, "id desc");
        List<FirmwareTaskDetailPo> list = taskDetailPoMapper.query(query);
        Integer total = taskDetailPoMapper.selectCount(query);
        return PageResDto.genResult(pageNo, limit, total, list, param);
    }
}
