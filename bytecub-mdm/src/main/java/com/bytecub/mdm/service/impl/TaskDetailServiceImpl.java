package com.bytecub.mdm.service.impl;

import com.bytecub.common.domain.gateway.mq.UpgradeMessageBo;
import com.bytecub.common.enums.UpgradeEnum;
import com.bytecub.mdm.dao.dal.FirmwareTaskDetailPoMapper;
import com.bytecub.mdm.dao.po.FirmwareTaskDetailPo;
import com.bytecub.mdm.service.ITaskDetailService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Date;
import java.util.List;

/**
 * com.bytecub.mdm.service.impl
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/14
 */
@Service
@Slf4j
public class TaskDetailServiceImpl implements ITaskDetailService {
    @Autowired
    private FirmwareTaskDetailPoMapper taskDetailPoMapper;

    @Override
    public String update(UpgradeMessageBo msg, UpgradeEnum upgradeEnum, String extMsg) {
        extMsg = StringUtils.isEmpty(extMsg) ? "" : extMsg;
        FirmwareTaskDetailPo query = new FirmwareTaskDetailPo();
        query.setMessageId(msg.getMessageId());
        List<FirmwareTaskDetailPo> list = taskDetailPoMapper.select(query);
        if(CollectionUtils.isEmpty(list)){
            FirmwareTaskDetailPo creator = new FirmwareTaskDetailPo();
            creator.setDeviceCode(msg.getDeviceCode());
            creator.setTaskId(msg.getTaskId());
            creator.setUpgradeStatus(upgradeEnum.getStatus());
            creator.setDetailMsg(upgradeEnum.getMsg()  + extMsg);
            creator.setMessageId(msg.getMessageId());
            taskDetailPoMapper.insertSelective(creator);
            return null;
        }

        FirmwareTaskDetailPo update = list.get(0);
        update.setDeviceCode(msg.getDeviceCode());
        update.setUpdateTime(new Date());
        update.setUpgradeStatus(upgradeEnum.getStatus());
        update.setDetailMsg(upgradeEnum.getMsg()  + extMsg);
        taskDetailPoMapper.updateByPrimaryKeySelective(update);
        return update.getFirmwareVersion();
    }
}
