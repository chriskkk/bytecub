package com.bytecub.mdm.dao.po;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Transient;
import javax.validation.constraints.NotNull;
import java.util.Date;
@Data
@Table(name="t_firmware")
public class FirmwarePo {
    @Id
    private Long id;
    @NotNull(message = "未上传附件")
    private String url;

    @NotNull(message = "版本号不能为空")
    private String firmwareVersion;
    @NotNull(message = "序列号不能为空")
    private Integer seqNo;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern ="yyyy-MM-dd HH:mm:ss",timezone ="GMT+8" )
    private Date createTime;
    @JsonFormat(shape = JsonFormat.Shape.STRING,pattern ="yyyy-MM-dd HH:mm:ss",timezone ="GMT+8" )
    private Date updateTime;
    @NotNull(message = "产品不能为空")
    private String productCode;

    private Integer delFlag;

    private String signType = "md5";
    @NotNull(message = "签名不能为空")
    private String signCode;
    @NotNull(message = "固件名不能为空")
    private String firmwareName;
    @NotNull(message = "升级模式不能为空")
    private Integer pushType;

    @Transient
    private String productName;
    @Transient
    private String fileBase64;


}