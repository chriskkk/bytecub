package com.bytecub.common.domain.dto.request.firmware;

import lombok.Data;

/**
 * com.bytecub.common.domain.dto.request.firmware
 * project bytecub
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/8
 */
@Data
public class FirmwareQueryReqDto {
    private String productCode;
    private String firmwareName;
}
