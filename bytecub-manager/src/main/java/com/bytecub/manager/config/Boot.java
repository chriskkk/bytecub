package com.bytecub.manager.config;

import com.bytecub.manager.mq.DelayUpgradeConsume;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;

/**
 * com.bytecub.manager.config
 * project bytecub  bytecub.cn
 *
 * @author songbin songbin.sky@hotmaial.com
 * @date 2021/4/29
 */
@Component
public class Boot {
    @Autowired
    private DelayUpgradeConsume delayUpgradeConsume;

    @PostConstruct
    public void init(){
        delayUpgradeConsume.consume();
    }
}
